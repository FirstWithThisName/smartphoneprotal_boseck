CALL createBluetooth("1.0");
CALL createBluetooth("2.0");
CALL createBluetooth("3.0");
CALL createBluetooth("4.0 Low-Power");

CALL createUser("user1", "test123", "test123@supermail.com");
CALL createUser("user2", "test123", "test1234@supermail.com");
CALL createUser("user3", "test123", "test1235@supermail.com");
CALL createUser("user4", "test123", "test1236@supermail.com");

CALL createChip("Snapdragon 845", NULL);
CALL createChip("Exynos 9810", "4 x Custom-ARM + 4 x ARM Cortex-A55
2,9 GHz");

CALL createChip("Exynos 5 Octa", "1,6 GHz Quad-Core Cortex-A15 und 1,2 GHz Quad-Core Cortex-A7, GPU: IT PowerVR SGX544MP3");

INSERT INTO wifiType(name) VALUES("802.11a");
INSERT INTO wifiType(name) VALUES("802.11n");
INSERT INTO wifiType(name) VALUES("802.11ac");

INSERT INTO manufacturer(name) VALUES("Samsung");
INSERT INTO manufacturer(name) VALUES("Google");
INSERT INTO manufacturer(name) VALUES("Apple");
INSERT INTO manufacturer(name) VALUES("Huawei");

INSERT INTO phone(name, prize,screenSize, pixelSolution, manufacurer_id, chip_id, bluetooth_id) VALUES("Galaxy S4", 350.0, "1920 × 1080 Pixel", "1080p", 1, 2, 3);
INSERT INTO phone(name, prize,screenSize, pixelSolution, manufacurer_id, chip_id, bluetooth_id) VALUES("Galaxy S5", 350.0, "1920 × 1080 Pixel", "1080p", 1, 2, 3);
INSERT INTO phone(name, prize,screenSize, pixelSolution, manufacurer_id, chip_id, bluetooth_id) VALUES("Galaxy S6", 350.0, "1920 × 1080 Pixel", "1080p", 1, 2, 3);
INSERT INTO phone(name, prize,screenSize, pixelSolution, manufacurer_id, chip_id, bluetooth_id) VALUES("Galaxy S7", 350.0, "1920 × 1080 Pixel", "1080p", 1, 2, 3);
INSERT INTO phone(name, prize,screenSize, pixelSolution, manufacurer_id, chip_id, bluetooth_id) VALUES("Iphone 8", 350.0, "1920 × 1080 Pixel", "1080p", 4, 2, 3);
INSERT INTO phone(name, prize,screenSize, pixelSolution, manufacurer_id, chip_id, bluetooth_id) VALUES("Iphone X", 350.0, "1920 × 1080 Pixel", "1080p", 4, 2, 3);

INSERT INTO report(text, phone_id, user_id) VALUES("das Handy ist ganz toll", 1, 1);

INSERT INTO os(name, version) VALUES("Android", "Éclair");
INSERT INTO os(name, version) VALUES("Window", "10");
INSERT INTO os(name, version) VALUES("Android", "Lollipop");
INSERT INTO os(name, version) VALUES("Android", "KitKat");

INSERT INTO news(value) VALUES("Christian hatte die Idee für einene NEWS Bereich");
INSERT INTO news(value) VALUES("Backend NEWS Bereich fettisch");
INSERT INTO news(value) VALUES("Christian das Frontend dafür");
INSERT INTO news(value) VALUES("der NEWS Bereich ist rosa");
INSERT INTO news(value) VALUES("der NEWS Bereich hat eine Scheiß Farbe");
