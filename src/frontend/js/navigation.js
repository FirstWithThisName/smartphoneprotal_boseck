class Navigator {
    constructor(elemnets_per_page, number_of_elements) {
        this.elemnetsPerPage = elemnets_per_page;
        this.numberOfElements = number_of_elements;
        this.page = 0;
    }

    back() {
        if (this.page <= 0) {
            return;
        }
        this.page--;
        clearPhones();
        showPhones(this.elemnetsPerPage, this.elemnetsPerPage * this.page);
    }

    forward() {
        this.page++;
        clearPhones();
        showPhones(this.elemnetsPerPage, this.elemnetsPerPage * this.page);
    }
}
