function showPhones(limit, offset) {
    $.getJSON('http://localhost/phone.php?limit=' + limit.toString() + '&offset=' + offset.toString(), function(data) {
        //console.log(data);
        displayPhones(data);
    });
}

function showNews(limit, offset) {
    $.getJSON('http://localhost/news.php?limit=' + limit.toString() + '&offset=' + offset.toString(), function(data) {
        var newsPart = document.createElement('div');
        for (var i = 0; i < data.length; i++) {
            var newsPart = document.createElement('div');
            newsPart.className = 'newspart';
            newsPart.innerHTML = "<h4><b>" + data[i].headline + "</b><h4>";
            newsPart.innerHTML += "<h5>" + data[i].created + "<h5>";
            newsPart.innerHTML += "<p>" + data[i].value + "</p>";
            document.getElementById('news').appendChild(newsPart);
        }
    });
}

function clearManufacturer() {
    var myNode = document.getElementById("manufacturer");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
}

function refreshManufacturer() {
    clearManufacturer();
    $.getJSON('http://localhost/manufacturer.php', function(data) {
        var select = document.getElementById('manufacturer');
        var option = document.createElement('option');
        option.innerHTML = 'alle';
        select.appendChild(option);
        for (var i = 0; i < data.length; i++) {
            var option = document.createElement('option');
            option.innerHTML = (data[i].name);
            select.appendChild(option);
        }
    });
}

function clearOS() {
    var myNode = document.getElementById("manufacturer");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
}

function refreshOS() {
    clearOS();
    $.getJSON('http://localhost/os.php', function(data) {
        var select = document.getElementById('os');
        var option = document.createElement('option');
        option.innerHTML = 'alle';
        select.appendChild(option);
        for (var i = 0; i < data.length; i++) {
            var option = document.createElement('option');
            option.innerHTML = (data[i].name);
            select.appendChild(option);
        }
    });
}


function clearPhones() {
    $('.phone').remove();
}

function displayPhones(data) {
    for (var i = 0; i < data.length; i++) {
        var newPhone = document.createElement('div');
        newPhone.className = 'phone';
        var text = document.createElement('div');
        text.className = "text";
        newPhone.appendChild(text);
        text.innerHTML += '<h3>' + data[i].name + '</h3>';
        if (data[i].description != null) {
            text.innerHTML += '<p>' + data[i].description + '</p>';
        }
        if (data[i].pictures.length > 0) {
            newPhone.innerHTML += "<image src=images/" + data[i].pictures[0] + ">";
        }
        document.getElementById('phones').appendChild(newPhone);
    }
}

function findPhone(name) {
    re = [];
    $.getJSON('http://localhost/phone.php?search=' + name, function(data) {
        clearPhones();
        displayPhones(data);
    });
    return re;
}
