<?php
    header("Content-Type: application/json; charset=UTF-8");
    include_once 'database/Database.php';
    include_once 'dataObjects/WifiType.php';

    function getWifiTypeById($os_id)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM wifiType where id = ?");
        $results = array();
        if ($stmt->execute(array($os_id))) {
            while ($row = $stmt->fetch()) {
                $re = new WifiType($row['id'], $row['name']);
                array_push($results, $re);
            }
        }
        return $results;
    }

    function getAllWifiTypes()
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM wifiType");
        $results = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $re = new WifiType($row['id'], $row['name']);
                array_push($results, $re);
            }
        }
        return $results;
    }

    $r = array();
    if (isset($_GET["wifitype_id"])) {
        $r = getWifiTypeById($_GET["wifitype_id"]);
    } else {
        $r = getAllWifiTypes();
    }
    echo json_encode($r);

 ?>
