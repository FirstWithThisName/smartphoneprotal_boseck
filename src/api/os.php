<?php

    header("Content-Type: application/json; charset=UTF-8");
    include_once 'database/Database.php';
    include_once 'dataObjects/OS.php';

    function getOSById($os_id)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM os where id = ?");
        $results = array();
        if ($stmt->execute(array($os_id))) {
            while ($row = $stmt->fetch()) {
                $re = new OS($row['id'], $row['name']);
                array_push($results, $re);
            }
        }
        return $results;
    }

    function getAllOS()
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM os");
        $results = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $re = new OS($row['id'], $row['name']);
                array_push($results, $re);
            }
        }
        return $results;
    }

    $r = array();
    if (isset($_GET["os_id"])) {
        $r = getOSById($_GET["os_id"]);
    } else {
        $r = getAllOS();
    }
    echo json_encode($r);

 ?>
