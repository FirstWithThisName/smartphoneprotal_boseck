<?php
// required headers
header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");

include_once 'Database.php';
//include_once '../dataObjects/Phone.php';



function getChips()
{
    $database = new Database();
    $sql = "SELECT * FROM chip";
    $results = array();
    $c = $database->getConnection();
    $result = $c->query($sql);
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
       array_push($results, new Chip($row['id'], $row['name'], $row['description']));
    }
    return $results;
}

function getPhones($limit, $offset)
{
    $sql = "SELECT * from phone LIMIT ".$limit." OFFSET " . $offset ;
    return phoneQuery($sql);
}

function getPhoneById($id)
{
    return phoneQuery("SELECT * FROM phone WHERE id=".$id);
}

function getPhoneByName($name) {
    $database = new Database();
    $dbh = $database->getConnection();
    $stmt = $dbh->prepare("SELECT * FROM phone where name LIKE ?");
    $results = array();
    if ($stmt->execute(array("%".$name."%"))) {
        while ($row = $stmt->fetch()) {
            $p = new Phone($row['id'],
                 $row['name'],
                 $row['releaseDay'],
                 $row['prize'],
                 $row['screenSize'],
                 $row['pixelSolution'],
                 $row['camera'],
                 $row['wight'],
                 $row['description'],
                 $row['manufacurer_id'],
                 $row['chip_id'],
                 $row['bluetooth_id']
            );
           array_push($results, $p);
        }
    }
    return $results;
}

function phoneQuery($sql)
{
    $database = new Database();
    $c = $database->getConnection();
    $result = $c->query($sql);
    $phones = parsePhoneQueryResult($result);
    foreach ($phones as $phone) {
        // $c = $database->getConnection();
        // $c->setAttribute( PDO::ATTR_EMULATE_PREPARES, false);
        $c->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $result = $c->query("SELECT path FROM phone_picture WHERE phone_id=" . $phone->id);
        if (!$result) {
            continue;
        }
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            array_push($phone->pictures, $row['path']);
        }

    }
    return $phones;
}



function parsePhoneQueryResult($result)
{
    $results = array();
    //echo var_dump($result);
    if (!isset($result) || is_null($result)) {
        return $restults;
    }
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $p = new Phone($row['id'],
             $row['name'],
             $row['releaseDay'],
             $row['prize'],
             $row['screenSize'],
             $row['pixelSolution'],
             $row['camera'],
             $row['wight'],
             $row['description'],
             $row['manufacurer_id'],
             $row['chip_id'],
             $row['bluetooth_id']
        );
       array_push($results, $p);
    }
    return $results;
}

function getReportsByPhone($phone_id) {
    $database = new Database();
    $dbh = $database->getConnection();
    $results = array();
    $stmt = $dbh->prepare("SELECT * FROM report where phone_id = ?");
    $results = array();
    if ($stmt->execute(array($phone_id))) {
        while ($row = $stmt->fetch()) {
            $r = new Report($row['id'],
                 $row['text'],
                 $row['created'],
                 $row['phone_id'],
                 $row['user_id']
            );
           array_push($results, $r);
        }
    }
    return $results;
}

function getReportsByUser($user_id)
{
    $database = new Database();
    $dbh = $database->getConnection();
    $results = array();
    $stmt = $dbh->prepare("SELECT * FROM report where user_id = ?");
    $results = array();
    if ($stmt->execute(array($user_id))) {
        while ($row = $stmt->fetch()) {
            $u = new Report($row['id'],
                 $row['text'],
                 $row['created'],
                 $row['phone_id'],
                 $row['user_id']
            );
           array_push($results, $u);
        }
    }
    return $results;
}
 ?>
