<?php
// TODO use a singelton
class Database {

    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "smartphoneportal_boseck";
    private $username = "mediaserver"; // TODO see requirements
    private $password = "test123";
    public $conn = null;

    // get the database connection
    public function getConnection(){

        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        } catch(PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}
?>
