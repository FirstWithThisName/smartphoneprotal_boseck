<?php

    header("Content-Type: application/json; charset=UTF-8");
    include_once 'database/Database.php';
    include_once 'dataObjects/User.php';

    function getUserById($user_id)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM user where id = ?");
        $results = array();
        if ($stmt->execute(array($user_id))) {
            while ($row = $stmt->fetch()) {
                $re = new User($row['id'], $row['name'], $row["email"]);
                array_push($results, $re);
            }
        }
        return $results;
    }

    function getAllUser()
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM user");
        $results = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $re = new User($row['id'], $row['name'], $row["email"]);
                array_push($results, $re);
            }
        }
        return $results;
    }

    $r = array();
    if (isset($_GET["user_id"])) {
        $r = getUserById($_GET["user_id"]);
    } else {
        $r = getAllUser();
    }
    echo json_encode($r);

 ?>
