<?php

    header("Content-Type: application/json; charset=UTF-8");
    include_once 'database/Database.php';
    include_once 'dataObjects/Manufacturer.php';

    function getManufacturerById($manufacturer_id)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM manufacturer where id = ?");
        $results = array();
        if ($stmt->execute(array($manufacturer_id))) {
            while ($row = $stmt->fetch()) {
                $re = new Manufacturer($row['id'], $row['name']);
                array_push($results, $re);
            }
        }
        return $results;
    }

    function getAllManufacturer()
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM manufacturer");
        $results = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $re = new Manufacturer($row['id'], $row['name']);
                array_push($results, $re);
            }
        }
        return $results;
    }

    $r = array();
    if (isset($_GET["manufacturer_id"])) {
        $r = getManufacturerById($_GET["manufacturer_id"]);
    } else {
        $r = getAllManufacturer();
    }
    echo json_encode($r);

 ?>
