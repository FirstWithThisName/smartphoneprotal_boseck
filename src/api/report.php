<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    include_once 'dataObjects/Phone.php';
    include_once 'dataObjects/Report.php';
    include_once 'database/read.php';

    $r = array();
    if (isset($_GET["phone_id"])) {
        $r = getReportsByPhone($_GET["phone_id"]);
    } else if  (isset($_GET["user_id"])) {
        $r = getReportsByUser($_GET["user_id"]);
    }

    echo json_encode($r);
 ?>
