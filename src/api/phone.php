<?php
	// required headers
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");

	include_once 'database/read.php';
	include_once 'dataObjects/Chip.php';
	include_once 'dataObjects/Phone.php';

	//$r = getPhones(2, 0);
	if (isset($_GET["phone_id"])) {
		$phoneId = $_GET["phone_id"];
		$r = getPhoneById($phoneId);
	} else if (isset($_GET["search"])) {
		$needle = $_GET["search"];
		$r = getPhoneByName($needle);
	} else {
		$limit = 20;
		$offset = 0;
		if (isset($_GET["offset"])) {
			$offset = $_GET["offset"];
		}

		if (isset($_GET["limit"])) {
			$limit = $_GET["limit"];
		}
		$r = getPhones($limit, $offset);
	}
	echo json_encode($r);
 ?>
