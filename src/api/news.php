<?php
    include_once 'database/Database.php';
    header("Content-Type: application/json; charset=UTF-8");

    /**
     *
     */
    class News
    {
        public $id;
        public $value;
        public $created;
        public $headline;

        function __construct($id, $value, $created, $headline)
        {
            $this->id = $id;
            $this->value = $value;
            $this->created = $created;
            $this->headline = $headline;
        }
    }

    function getNews($limit, $offset)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        // is this a hack ?
        $dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM news ORDER BY created DESC LIMIT ? OFFSET ?");
        $results = array();
        if ($stmt->execute(array($limit, $offset))) {
            while ($row = $stmt->fetch()) {
                $re = new News($row['id'], $row['value'], $row["created"], $row["headline"]);
                array_push($results, $re);
            }
        } else {
            echo "wtf";
        }
        return $results;
    }

    $r = array();
    $limit = 3;
    $offset = 0;
    if (isset($_GET["offset"])) {
        $offset = $_GET["offset"];
    }
    if (isset($_GET["limit"])) {
        $limit = $_GET["limit"];
        // maximum limit of 20
        if ($limit > 20) {
            $limit = 20;
        }
        if ($limit < 0) {
            $limit = 1;
        }
    }
    $r = getNews($limit, $offset);
    echo json_encode($r);
 ?>
