<?php

    header("Content-Type: application/json; charset=UTF-8");
    include_once 'database/Database.php';
    include_once 'dataObjects/Comment.php';
    include_once 'dataObjects/Vote.php';

    function getCommentByReportId($report_id)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM comment where report_id = ?");
        $results = array();
        if ($stmt->execute(array($report_id))) {
            while ($row = $stmt->fetch()) {
                $re = new Comment($row['id'],
                    $row['text'],
                    $row["created"],
                    $row["user_id"],
                    $row["report_id"]
                );
                getVoting($re);
                array_push($results, $re);
            }
        }
        return $results;
    }

    function getVoting(&$comment)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM comment_voting where comment_id = ?");
        $results = array();
    
        if ($stmt->execute(array($comment->id))) {
            while ($row = $stmt->fetch()) {
                $re = new Vote(
                    $row['mark'],
                    $row["user_id"]
                );
                //print(var_dump($re))
                getVoting($re);
                array_push($comment->voting, $re);
            }
        }
    }

    function getAllComment()
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM comment");
        $results = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $re = new Comment($row['id'],
                    $row['text'],
                    $row["created"],
                    $row["user_id"],
                    $row["report_id"]
                );
                getVoting($re);
                array_push($results, $re);
            }
        }
        return $results;
    }

    $r = array();
    if (isset($_GET["report_id"])) {
        $r = getCommentByReportId($_GET["report_id"]);
    } else {
        $r = getAllComment();
    }
    echo json_encode($r);

 ?>
