<?php

    header("Content-Type: application/json; charset=UTF-8");
    include_once 'database/Database.php';
    include_once 'dataObjects/Bluetooth.php';

    function getBluetoothById($bluetooth_id)
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM bluetooth where id = ?");
        $results = array();
        if ($stmt->execute(array($bluetooth_id))) {
            while ($row = $stmt->fetch()) {
                $re = new Bluetooth($row['id'],$row["version"]);
                array_push($results, $re);
            }
        }
        return $results;
    }

    function getAllBluetooth()
    {
        $database = new Database();
        $dbh = $database->getConnection();
        $results = array();
        $stmt = $dbh->prepare("SELECT * FROM bluetooth");
        $results = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $re = new Bluetooth($row['id'], $row['version']);
                array_push($results, $re);
            }
        }
        return $results;
    }

    $r = array();
    if (isset($_GET["bluetooth_id"])) {
        $r = getBluetoothById($_GET["bluetooth_id"]);
    } else {
        $r = getAllBluetooth();
    }
    echo json_encode($r);


 ?>
