<?php
    /**
     *
     */
    class Report
    {
        public $id;
        public $text;
        public $created;
        public $phone_id;
        public $user_id;

        function __construct($id, $text, $created, $phone_id, $user_id)
        {
            $this->id = $id;
            $this->text = $text;
            $this->created = $created;
            $this->phone_id = $phone_id;
            $this->user_id = $user_id;
        }
    }

 ?>
