<?php
/**
 *
 */

class Phone
{
    public $id;
    public $name;
    public $releaseDay;
    public $prize;
    public $screenSize;
    public $pixelSolution;
    public $camera;
    public $wight;
    public $description;
    public $manufacurer;
    public $chip;
    public $bluetooth;
    public $pictures;

    public function __construct($id, $name, $releaseDay, $prize, $screenSize, $pixelSolution, $camera, $wight, $description, $manufacurer, $chip, $bluetooth)
    {
        $this->id = $id;
        $this->name = $name;
        $this->releaseDay = $releaseDay;
        $this->prize = $prize;
        $this->screenSize = $screenSize;
        $this->pixelSolution = $pixelSolution;
        $this->camera = $camera;
        $this->wight = $wight;
        $this->description = $description;
        $this->manufacurer = $manufacurer;
        $this->chip = $chip;
        $this->bluetooth = $bluetooth;
        $this->pictures = array();
    }
}

 ?>
