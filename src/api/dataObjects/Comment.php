<?php

    /**
     *
     */
    class Comment
    {
        public $id;
        public $text;
        public $created;
        public $user_id;
        public $report_id;
        public $voting;

        function __construct($id, $text, $created, $user_id, $report_id)
        {
            $this->id = $id;
            $this->text = $text;
            $this->created = $created;
            $this->user_id = $user_id;
            $this->report_id = $report_id;
            $this->voting = array();
        }
    }
?>
